#include "strrev.h"

#include <stdio.h>
#include <string.h>

void reverse(char* str) {
	char* begin = str;
	char* end = str + strlen(str) - 1;
	while (begin < end) {
		char c = *end;
		*end = *begin;
		*begin = c;
		begin++;
		end--;
	}
}
